//==> if conditon
// var i = 10;
// if(i>15){
//     document.write("condition is true");
// }
// else{
//   document.write("condition is false")
// }
//===> nested if
// var i=50,j=20,k=30;
// if(i>j && i>k){
//     console.log("i is grater");
// }
// else if(j>i && j>k){
//     console.log("j is grater")
// }
// else{
//     console.log("k is grater")
// }
// -----------------------------------
//==>swich case
// var i =10;
// switch(i){
//     case 1:
//         console.log("i is 1");
//         break;
//     case 2:
//         console.log("i is 2");
//         break;
//         default:
//             console.log("i is grater");
//             break;
// }
// ---------------------------------------------
// for loop
// var i;
// var count =0;
// for(i=0; i<10; i++){
//     console.log( count + "hellow.world!!!!" );
//     count++;
// }
// console.log(count);
// -------------------------------------------------
// for in
// var obj = {name: "rahul",name2: "himanshu", name3: "ashutosh", name4: "nitish", name5: "sonam" }
// for(x in obj){
//     console.log(obj[x]);
// }
// ----------------------------------------------------
// infinite loop
// var i;
// while(true){
//     console.log("ram")
// }
// let i = 0;

// while (i < 6) {
//   if (i === 3) {
//     break;
//   }
//   i = i + 1;
// }

// console.log(i);
// -------------------------------------------------------
// function
// function fun(name1,name){
//     console.log(name1,name);
// }
// fun("rahul","yadav");
// -----------------------------------------------------------
//call function with help of button
// function First() { 
//     var a = 123; 
//     var b = 145; 
//     var c = a + b; 
//     alert("Value of a: " + a ); 
//     alert("Value of b: " + b ); 
//     alert("Sum of a and b: " + c); 
//   }
//------------------------------------------------------------
// let school = { 
//     name: 'Vivekananda School', 
//     location : 'Delhi', 
//     established : '1971', 
//     displayInfo : function(){ 
//         console.log(`${school.name} was established 
//                                   in ${school.established} at ${school.location}`); 
//     } 
// } 
// school.displayInfo();
// ------------------------------------------------------------------
// abc();
// var abc = function fun(){
//     console.log("hello");
// };
// ---------------------------------------------------------------------------
// var myfun = (a) => {
//     console.log(a);
// }
// myfun(10);

// var obj = {a:'custom'}; var a="global"; function what()
// {
//     console.log(this.a)
// // return this.a;
// } what();


// function rev(str)
// {

//     console.log(str.split("").reverse().join(""));

// }
// rev("this is the person")

// var mtfunction = c => c;
// console.log(mtfunction(0));
//------------------------------------------
// WAP for pranthisis ????????????

// let abc = function (str) {
//     let stack = [];
//     let map = {
//         '(': ')',
//         '[': ']',
//         '{': '}'
//     }
//     for (let i = 0; i < str.length; i++) {

//         if (str[i] === '(' || str[i] === '{' || str[i] === '[' ) {
//             stack.push(str[i]);
//         }
        
//         else {
//             let last = stack.pop();

            
//             if (str[i] !== map[last]) {return false};
//         }
//     }
//         if (stack.length !== 0) {return false};

//     return true;
// }

// console.log(abc("(){}{}()"));
//---------------------------------------------------------
//24/02/2020
// var a = "rahul";
// console.log(a);
// this.a = "RAHUL";
// console.log(a);
//--------------------------------------------------------

// Function that contains this. 
// function myFunc() { 
//     console.log( this.a ); 
//   } 
//   var a = "Global";   
//   // Owner of the function. 
//   var myObj1 = { 
//     a: "myObj1", 
//     myFunc: myFunc 
//   };   
//   // Object other than the owner. 
//   var myObj2 = { 
//     a: "myObj2"
//   }; 
//  // Call the function in Global Scope. 
//   myFunc();   
//   // Call the function from the reference of owner.  
//   myObj1.myFunc();   
//   // Call the function from the reference 
//   // of object other than the owner. 
//   myFunc.call( myObj2 );   
//   // Create a new undefined object. 
//   new myFunc(); 
// -------------------------------------------------------
// Object
// var obj = { 
//     rahul: 100,
//     yadav: 500
//  };
//    obj.abc = 125

// console.log( obj.rahul + "\n" + obj.yadav + obj.abc);
// ---------------------------------------------------
// function doSome(){}
// doSome.prototype.foo="bar";
// var doinstance= new doSome();
// doinstance.prop="some value";
// console.log(doinstance.prop);//some value
// console.log(doinstance.foo);//bar
// console.log(doSome.prop);//undefine
// console.log(doSome.foo);//undefine
// console.log(doSome.prototype.foo);//bar
// console.log(doSome.prototype.prop);//undefine
// --------------------------------------------------------
//FACTARY FUNCTION
// function fac(){
//     return{
//         model: "galexy"
//     }
// }

// var a = {a: 1}; 

// var b = Object.create(a);
// console.log(b.a); 
// console.log(this.a);

// var c = Object.create(null);
// console.log(c.a);

// c.a= 4;

// var d = Object.create(c);
// console.log(d.a);

// var e = Object.create(d);
// console.log(e.a);
//-------------------------------
// var Employee = {
//     age: 28,
//     name: 'abc',
//     name1: 'the',
//     salary: 1023025,
//     designation: 'developer'
//   } 
//   console.log(delete Employee.name); 
//   console.log(Employee.name1);  
//   console.log(delete Employee.age);    
//   console.log(delete Employee.salary); 
//----------------------------------------
// var pappu = "himanshu";
// function tappu(){
//     return{
//         lappu: "kappu",
//         nappu: "cappu"
//     };
// }
// var lappu = tappu();
// console.log(lappu.nappu)
//-------------------------------------------------------------
// WAP to Array
// let object = { a: [1, 2, 3], b: [2, 3], c: [1, 3, 5] }

// let objectArr = [];

// for (key in object)
// objectArr.push(...object[key]);

// let finalObject = {};

// objectArr.forEach(el => {

// if (!finalObject[el])
// finalObject[el] = [];
// })
// // console.log(finalObject);
// for (key in finalObject) {
// // console.log(key);
// for (childkey in object) {
// // console.log(childkey);
// const element = object[childkey].find(el => el == key);
// if (element)
// finalObject[key].push(childkey);
// }
// }

// console.log(finalObject);
// __________________________________________________
// var number = "rahul47sfdsf8dssa2",
//     output = [],
//     sNumber = number.toString();

// for (var i = 0, len = sNumber.length; i < len; i += 1) {
//     output.push(+sNumber.charAt(i));      

// if(output == NaN){
//     output =0;           //use str_val at the place you want
//    // console.log(str_val);

// }
// }   
// console.log(output);

// var str_val = ('#itemscounter45rety6431');

// if(str_val == NaN){
//    str_val =0;           //use str_val at the place you want
//    console.log(str_val);
// }
// whole_string= "82eser45rwe51d4r4"
// split_string = whole_string.split(/(\d+)/)
// console.log(split_string[1])
// //______________________________________
//WAP TO SAPRATE NUMBER IN A GIVEN STRING
// "use strict"
// let abc = "ahdf45fdf14frf52";
// let arr = [];
// let reg = (/\d+/g);
// let result = abc.match(reg);
// console.log(result);
// for(var i=0; i<result.length; i++){
//  arr.push(parseInt(result[i]));
// }
// console.log(arr);
// var a = arr.reduce((a,b) => a+b)
// console.log(a);

//_________________________________________
// or
// function sumDigitsFromString(str) {
//     var sum = 0;
//     var numbers = str.match(/\d+/g).map(Number);
//     for (var i = 0; i < numbers.length; i++) {
//     sum += numbers[i]
//     }
//     console.log(sum);
//     }
//     sumDigitsFromString('12gddjhj5hgfhj6hd11')                                 
// ____________________________________________________________________________________________________
//paranthisis

// let abc = function (str) {
//     let stack = [];
//     let map = {
//         '(': ')',
//         '[': ']',
//         '{': '}'
//     }
//     for (let i = 0; i < str.length; i++) {
//         if (str[i] === '(' || str[i] === '{' || str[i] === '[' ) {
//             stack.push(str[i]);
//         }        
//         else {
//             let last = stack.pop();            
//             if (str[i] !== map[last]) {return false};
//         }
//     }
//         if (stack.length !== 0) {return false};
//     return true;
// }

// console.log(abc("{{{()}"));
// ________________________________________________________
// Given two numbers, return true if the sum of both numbers is less than 100. Otherwise return false.

// var a = 100;
// var b = 20;
// var c = a+b;
// if(c<100){
//     console.log("true");
// }
// else{
//     console.log("false");
// }
// ____________________________________________________________________________
// Create a function that searches for the index of a given item in an array. 
// If the item is present, it should return the index, otherwise, it should return -1.

// let arr = ['x','z',1,2,3,4,'a','b']
// function checkIndex(){
//     let char = 'k';
//     if(arr.indexOf(char) != -1)
//     console.log(arr.indexOf(char));
//     else
//     return -1;
// }
// checkIndex();
// ____________________________________________________
//fabonacci series

// var r = 0;
// var t = 1;
// console.log(r)
// console.log(t)
// var n = 8;
// for(var i = 2; i<=n; i++){
//   var   e = r + t;
//         r = t;
//         t = e;
//         console.log(e)
// }
// /--------------------------------------
//or

// var r = 0;
// var t = 1;
// var n = 8;
// for(var i = 2; i<=n; i++){
//     console.log(r)
//     var e = r + t;
//         r = t;
//         t = e;
        
// }
// ____________________________________________________
// print sq of every number without js function
// for ex 8119--641181

// var arr = "8119"
// for(var i=0; i<arr.length; i++){
//     var c = arr[i]*arr[i];
//     console.log(c)
// }
// //_______________________________________________________
// or

// var n = 8119
// var r = 0;
// while (n != 0)
//   {
//     r = r * 10;
//     r = r + n % 10;
//     n = n/10;
//   }
// console.log(r);
// // ___________________________________________________
//Reverse program

//    var  number = 245
//     var reversed = 0;
//     var temp = number;
//      while (number != 0) {
//      reversed *= 10;
//      reversed += number % 10;
//      number -= number % 10;
//      number /= 10;
//     }         
    
//     function pall(str) {
//     let revString = '';
//     for (let char of str) {
//     revString = char + revString;
//     }
    
//     return revString === str ? true : false;
//     }
//     console.log(pall('madam'));

//      console.log("not palindrom")
 
//___________________________________________________________
// WAP or square

//    var  number = 245
//     var reversed = 0;
//     var numStr = " "
//     var temp = number;
//      while (number != 0) {
//      reversed = reversed*10;
//      reversed = reversed+number%10;
//      number -= number % 10;
//      number /= 10;
//     }  
//     console.log(reversed)       
//     while(reversed > 0){
//        var  digit = reversed % 10;
//         multiply = digit * digit;
//         reversed = parseInt(reversed / 10);
//         numStr += multiply;
//     }
//     console.log(numStr);
// _____________________________________________________________________________
//palindorm in a string

// function pall(str) {
//     let revString = '';
//     for (let char of str) {
//     revString = char + revString;
//     }
    
//     return revString === str ? true : false;
//     }
//     console.log(pall('madam'));
// _____________________________________________________________________________
// function array_diff(a, b) {
//     let difArray = []
//     for ( let i = 0; i < a.length; i++) {
//         let elem = a[i]
//         if (b.indexOf(elem) === -1 ){
//          difArray.push(elem)
//         }
//     }
//     console.log(difArray)
//   }
//    array_diff([1,2],[1])
//    array_diff([1,2,2,2,3],[2])
//-----------------------------------------------------
// longest word in array with function
// function findLongestWord(str) {
//     var strSplit = str.split(' ');
//     var longestWord = 0;
//     for(var i = 0; i < strSplit.length; i++){
//       if(strSplit[i].length > longestWord){
//       longestWord = strSplit[i].length;
//        }
//     }
//     console.log(longestWord);
//   }
//   findLongestWord("The quick brown fox jumpedw over the lazy dog");
// ___________________________________________________________________
// or Without Function find largest word length
// var str = "I am a Developer and an Intern";
// var result = 0;
// var current = 0;
// for(var i = 0 ; i < str.length ; i++ ){
//     if(str[i] != ' '){
//         current++;
//     }
//     else{
//         if(result >= current){
//             result = result;
//         }
//         else{
//             result = current;
//         }
//         current = 0;
//     }
// }
// if (result >= current) {
//     result = result;
// }
// else {
//     result = current;
// }
// console.log(result);
//______________________________________________________________________________
// or With Function find largest word name
// function findLongestWord(str) {
//     const longestWord = str.split(' ').sort(function(a, b) {
//       return b.length - a.length;
//     });
//     return longestWord[0];
//   }
//   console.log(findLongestWord('i am a developer and an intern'));
// // _____________________________________________________________________________________
// or
// let str = 'i am a developer'; //['i', 'am', 'a', 'developer']
// const arredStr = str.split(' ');
// console.log(arredStr);

// function longestStringReduce(arr) {
//   return arr.reduce((a, b) => (a.length < b.length ? b : a), '');
// }

// console.log(longestStringReduce(arredStr));
// _______________________________________________________________________________
//without function
// var str = 'hi i am a developer and superhero and an intern';
// var newStr = str.match(/[a-z]+/g);
// // console.log(newStr);
// var largest = ' ';
// for(var i = 0 ; i < newStr.length ; i++){
// // console.log(newStr[i].length);
// if(newStr[i].length > largest.length){
// largest = newStr[i];
// }
// if (newStr[i].length == largest.length) {
// largest1 = newStr[i];
// }
// }
// console.log(largest ," ", largest1);
// _____________________________________________________________________________
//WAP TO Leap year or not
// function leapyear(year)
// {
// return (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0);
// }
// console.log(leapyear(2015));
//____________________________________________________________________
// how to remove white space 
// var rs = function ( text ) {
// 	var result = "";	
// 	var prevChar = " ";
// 	for ( var i = 0; i < text.length; i++ ) {		
// 		var currentChar = text[ i ];	
// 		if ( !( prevChar == " " && currentChar == prevChar ) ) // if it's not space followed by another space
// 			result += currentChar; // append			
// 		prevChar = currentChar; // update prevChar
// 	}		console.log(result);
// }
// rs(" Programming  is              fun      !");
//____________________________________________________________________
// or
// let b = [];
// a = ' hey aasas ';

// for (let i = 0; i < a.length; i++) {
// b.push(a[i]);
// }
// let newStr = '';
// for (let i = 0; i < b.length; i++) {
// if (b[i] !== ' ') {
// newStr = newStr + b[i];
// }}
// console.log(newStr);
//____________________________________________________________________________

// var row = 5;
// var col = 5;
// for (var i=1; i<=row; i++){
//   for (var j=i; j<=col; j++){
//     console.log("*");
//   }
  
// }

// ____________________________________________________________________________
// let y = 10;
// let x = 10;

// let str = "";

// for(let i = 1; i < y; i++ ){
//     for(let j = 1; j < x; j++){
//         if(i + j >= y){
//             str = str.concat("*");
//         }else{
//             str = str.concat(" ")
//         }
//     }
//     str = str.concat("\n")
// }

// console.log(str)
// ____________________________________________________________________
// Wap check Perfact Square or not
// function findNextSquare(sq) {
//     // Return the next square if sq if a perfect square, -1 otherwise
//     if (!isSquare(sq)) {
//       return -1;
//     }
//     do {
//       sq++;
//     } while (!isSquare(sqlet str = "middle";  //'middle','test','testing'
// let n = str.length;
// if(n % 2 != 0){
//     console.log(str[parseInt(n/2)]);
// }
// else{
//     console.log(str[parseInt((n / 2) - 1)] + str[parseInt(n / 2)]);
// }));
//     console.log(sq);
//   }
//   const isSquare = (sq) => { 
//     const rt = Math.sqrt(sq); 
//  console.log(rt == Math.floor(rt)); 
//   }
//   findNextSquare(144)
// ___________________________________________________________________________
//You are going to be given a word. Your job is to return the middle character of the word. 
// If the word's length is odd, return the middle character. 
// If the word's length is even, return the middle 2 characters.
// let str = "middle";  //'middle','test','testing'
// let n = str.length;
// if(n % 2 != 0){
//     console.log(str[parseInt(n/2)]);
// }
// else{
//     console.log(str[parseInt((n / 2) - 1)] + str[parseInt(n / 2)]);
// }
// ________________________________________________________________________________________________
// Check to see if a string has the same amount of 'x's and 'o's. The method must return a boolean and be case insensitive. 
// The string can contain any char.
// function abc(s){
//     countX = 0;
//     countO = 0;
//     for(let i=0; i<s.length; i++)
//     {
//         if((s[i] == 'X') || (s[i] == 'x'))
//         {
//             countX = countX + 1;
//         }
//         else if((s[i] == 'O') || (s[i] == 'o'))
//         {
//             countO = countO + 1;
//         }
//     }
//     if(countX == countO)
//     {
//         console.log("true");
//     }
//     else
//     {
//         console.log("false");

//     }
// };
// abc("xooxxo");
// _____________________________________________________________________________
// Multiple two matrix
// function abc(m1, m2) {
//     var result = [];
//     for (var i = 0; i < m1.length; i++) {
//         result[i] = [];
//         for (var j = 0; j < m2[0].length; j++) {
//             var sum = 0;
//             for (var k = 0; k < m1[0].length; k++) {
//                 sum += m1[i][k] * m2[k][j];
//             }
//             result[i][j] = sum;
//         }
//     }
//     return result;
// }

// var m1 = [[1,2],[3,4]]
// var m2 = [[5,6],[7,8]]

// var mResult = abc(m1, m2)

// console.log(mResult)
// ___________________________________________________________________________________
// OR
// mA = [
//     [1, 2, 3],
//     [4, 5, 6],
//     [9, 3, 19]
//     ];
//     mB = [
//     [7, 8,3],
//     [9, 10,3],
//     [11, 12,5]
//     ];
//     function multiplyMat(mA, mB) {
//     let result = new Array(mA.length);
    
//     for (let i = 0; i < result.length; i++) {
//     result[i] = new Array(mB[i].length);
//     for (let j = 0; j < mB[i].length; j++) {
//     result[i][j] = 0;
//     for (let k = 0; k < mB.length; k++) {
//     result[i][j] += mA[i][k] * mB[k][j];
//     }
//     }
//     }
//     return result;
//     }
    
//     console.log(multiplyMat(mA, mB));
// ___________________________________________________
//SPIRAL MATRIX:-
// function matrix(n) {
//     // let result = new Array(n).fill().map(() => new Array(n).fill(''));
//     let result = [];
//     for (let i = 0; i < n; i++) {
//         result.push([])
//     }
//     console.log(result);
//     let counter = 1;
//     let startCol = 0;
//     let endCol = n - 1;
//     let startRow = 0;
//     let endRow = n - 1;
//     while (startCol <= endCol && startRow <= endRow) {
//         for (let i = startCol; i <= endCol; i++) {
//             result[startRow][i] = counter;
//             counter++;
//         }
//         startRow++;
//         for (let j = startRow; j <= endRow; j++) {
//             result[j][endCol] = counter;
//             counter++;
//         }
//         endCol--;
//         for (let i = endCol; i >= startCol; i--) {
//             result[endRow][i] = counter;
//             counter++;
//         }
//         endRow--;
//         for (let i = endRow; i >= startRow; i--) {
//             result[i][startCol] = counter;
//             counter++;
//         }
//         startCol++;
//     }
//     console.log('result: \n', result);
//     return result;
// }
// matrix(4);
// _______________________________________________________________________________
// var m1 = [[1,2],[3,4]]
// var m2 = [[5,6],[7,8]]
//     var result = [];
//     for (var i = 0; i <2 ; i++) {
//         result[i] = [];
//         for (var j = 0; j < 2; j++) {
//             var sum = 0;
//             for (var k = 0; k < 2; k++) {
//                 sum = sum + m1[i][k] * m2[k][j];
//             }
//             result[i][j] = sum;
//         }
//     }
//     console.log(result);
// _________________________________________________________________________________________
// How to find the longest consecutive set of numbers in a Javascript array
// 'use strict';

// const numbers = [1,6,7,8,9,10, 21, 22, 24, 26, 27, 28, 30, 38, 42, 43, 49, 52, 61, 65, 66       ];
// let chunks = [];
// let prev = 0;
// numbers.forEach((current) => {  
//     if(current ===1){
//         chunks.push([]);
//     }
//   if ( current - prev != 1 ) chunks.push([]); 
//   chunks[chunks.length - 1].push(current);
//   prev = current;
// });
// chunks.sort((a, b) => b.length - a.length);
// console.clear();
// console.log('Longest consecutive set:', chunks[0]);
// console.log('Size of longest consecutive set:', chunks[0].length);
// ___________________________________________________________________________________________
// Binary Search
// 	   const arr = [2, 4, 8, 10, 12];
// 		var first = 0;
// 		var last = arr.length - 1;
// 		var middle = (first + last) / 2;
// 		var search = 8;
// 		while (first <= last) {
// 			if (arr[middle] < search)
// 				first = middle + 1;
// 			else if (arr[middle] == search) {
// 				console.log(search + " found at location " + (middle + 1) + ".");
// 				break;
// 			} else
// 				last = middle - 1;
// 			middle = (first + last) / 2;
// 		}
// 		if (first > last)
// 			console.log(search + " isn't present in the list.\n");
// ______________________________________________________________________________________________
// Return the string reverse the charc grater than 5
// var arr = "enjoy the silenc";
// var str1 = arr.match(/[a-z]+/g);
// for(i=0; i<str1.length; i++){
//     if(str1[i].length>=5){
//      var reverse = str1[i].split("").reverse().join("");
//     }
//     console.log(reverse);

// if(str1[i].length>=5)
// {
//     str1[i]=reverse;
// }
// }
// console.log(str1.join(" "));
// __________________________________________________________
// find the missing number
// var num = [1,2,4,5,6,7]
// for(i=0; i<num.length; i++)
// {
//     // console.log(num[i])
//     if(num[i] + 1 !== num[ i + 1 ]){
//     var a = num[i] + 1; 
//     break;
//     }   
// }
// console.log(a); 
// __________________________________________________________________________
//     Amstrong number 
//     var result = 0;
//     var Num = 153;
//     var  num = Num;
//     while (Num !== 0) {
//         remainder = Num % 10;
//         result += remainder * remainder * remainder;
//         Num = parseInt(Num/10);
//     }
//     if (result == num)
//         console.log("Armstrong number.", num);
//     else
//         console.log("not an Armstrong number.", num);
// _______________________________________________________________________________

    
