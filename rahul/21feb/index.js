//==> if conditon
// var i = 10;
// if(i>15){
//     document.write("condition is true");
// }
// else{
//   document.write("condition is false")
// }
//===> nested if
// var i=50,j=20,k=30;
// if(i>j && i>k){
//     console.log("i is grater");
// }
// else if(j>i && j>k){
//     console.log("j is grater")
// }
// else{
//     console.log("k is grater")
// }
// -----------------------------------
//==>swich case
// var i =10;
// switch(i){
//     case 1:
//         console.log("i is 1");
//         break;
//     case 2:
//         console.log("i is 2");
//         break;
//         default:
//             console.log("i is grater");
//             break;
// }
// ---------------------------------------------
// for loop
// var i;
// var count =0;
// for(i=0; i<10; i++){
//     console.log( count + "hellow.world!!!!" );
//     count++;
// }
// console.log(count);
// -------------------------------------------------
// for in
// var obj = {name: "rahul",name2: "himanshu", name3: "ashutosh", name4: "nitish", name5: "sonam" }
// for(x in obj){
//     console.log(obj[x]);
// }
// ----------------------------------------------------
// infinite loop
// var i;
// while(true){
//     console.log("ram")
// }
// let i = 0;

// while (i < 6) {
//   if (i === 3) {
//     break;
//   }
//   i = i + 1;
// }

// console.log(i);
// -------------------------------------------------------
// function
// function fun(name1,name){
//     console.log(name1,name);
// }
// fun("rahul","yadav");
// -----------------------------------------------------------
//call function with help of button
// function First() { 
//     var a = 123; 
//     var b = 145; 
//     var c = a + b; 
//     alert("Value of a: " + a ); 
//     alert("Value of b: " + b ); 
//     alert("Sum of a and b: " + c); 
//   }
//------------------------------------------------------------
// let school = { 
//     name: 'Vivekananda School', 
//     location : 'Delhi', 
//     established : '1971', 
//     displayInfo : function(){ 
//         console.log(`${school.name} was established 
//                                   in ${school.established} at ${school.location}`); 
//     } 
// } 
// school.displayInfo();
// ------------------------------------------------------------------
// abc();
// var abc = function fun(){
//     console.log("hello");
// };
// ---------------------------------------------------------------------------
// var myfun = (a) => {
//     console.log(a);
// }
// myfun(10);

// var obj = {a:'custom'}; var a="global"; function what()
// {
//     console.log(this.a)
// // return this.a;
// } what();


// function rev(str)
// {

//     console.log(str.split("").reverse().join(""));

// }
// rev("this is the person")

// var mtfunction = c => c;
// console.log(mtfunction(0));
//------------------------------------------
// WAP for pranthisis ????????????

// let abc = function (str) {
//     let stack = [];
//     let map = {
//         '(': ')',
//         '[': ']',
//         '{': '}'
//     }
//     for (let i = 0; i < str.length; i++) {

//         if (str[i] === '(' || str[i] === '{' || str[i] === '[' ) {
//             stack.push(str[i]);
//         }
        
//         else {
//             let last = stack.pop();

            
//             if (str[i] !== map[last]) {return false};
//         }
//     }
//         if (stack.length !== 0) {return false};

//     return true;
// }

// console.log(abc("(){}{}()"));
//---------------------------------------------------------
//24/02/2020
// var a = "rahul";
// console.log(a);
// this.a = "RAHUL";
// console.log(a);
//--------------------------------------------------------

// Function that contains this. 
// function myFunc() { 
//     console.log( this.a ); 
//   } 
//   var a = "Global";   
//   // Owner of the function. 
//   var myObj1 = { 
//     a: "myObj1", 
//     myFunc: myFunc 
//   };   
//   // Object other than the owner. 
//   var myObj2 = { 
//     a: "myObj2"
//   }; 
//  // Call the function in Global Scope. 
//   myFunc();   
//   // Call the function from the reference of owner.  
//   myObj1.myFunc();   
//   // Call the function from the reference 
//   // of object other than the owner. 
//   myFunc.call( myObj2 );   
//   // Create a new undefined object. 
//   new myFunc(); 
// -------------------------------------------------------
// Object
// var obj = { 
//     rahul: 100,
//     yadav: 500
//  };
//    obj.abc = 125

// console.log( obj.rahul + "\n" + obj.yadav + obj.abc);
// ---------------------------------------------------
// function doSome(){}
// doSome.prototype.foo="bar";
// var doinstance= new doSome();
// doinstance.prop="some value";
// console.log(doinstance.prop);//some value
// console.log(doinstance.foo);//bar
// console.log(doSome.prop);//undefine
// console.log(doSome.foo);//undefine
// console.log(doSome.prototype.foo);//bar
// console.log(doSome.prototype.prop);//undefine
// --------------------------------------------------------
//FACTARY FUNCTION
// function fac(){
//     return{
//         model: "galexy"
//     }
// }

// var a = {a: 1}; 

// var b = Object.create(a);
// console.log(b.a); 
// console.log(this.a);

// var c = Object.create(null);
// console.log(c.a);

// c.a= 4;

// var d = Object.create(c);
// console.log(d.a);

// var e = Object.create(d);
// console.log(e.a);
//-------------------------------
// var Employee = {
//     age: 28,
//     name: 'abc',
//     name1: 'the',
//     salary: 1023025,
//     designation: 'developer'
//   } 
//   console.log(delete Employee.name); 
//   console.log(Employee.name1);  
//   console.log(delete Employee.age);    
//   console.log(delete Employee.salary); 
//----------------------------------------
// var pappu = "himanshu";
// function tappu(){
//     return{
//         lappu: "kappu",
//         nappu: "cappu"
//     };
// }
// var lappu = tappu();
// console.log(lappu.nappu)
//-------------------------------------------------------------
// WAP to Array
// let object = { a: [1, 2, 3], b: [2, 3], c: [1, 3, 5] }

// let objectArr = [];

// for (key in object)
// objectArr.push(...object[key]);

// let finalObject = {};

// objectArr.forEach(el => {

// if (!finalObject[el])
// finalObject[el] = [];
// })
// // console.log(finalObject);
// for (key in finalObject) {
// // console.log(key);
// for (childkey in object) {
// // console.log(childkey);
// const element = object[childkey].find(el => el == key);
// if (element)
// finalObject[key].push(childkey);
// }
// }

// console.log(finalObject);
// __________________________________________________
// var number = "rahul47sfdsf8dssa2",
//     output = [],
//     sNumber = number.toString();

// for (var i = 0, len = sNumber.length; i < len; i += 1) {
//     output.push(+sNumber.charAt(i));      

// if(output == NaN){
//     output =0;           //use str_val at the place you want
//    // console.log(str_val);

// }
// }   
// console.log(output);

// var str_val = ('#itemscounter45rety6431');

// if(str_val == NaN){
//    str_val =0;           //use str_val at the place you want
//    console.log(str_val);
// }
// whole_string= "82eser45rwe51d4r4"
// split_string = whole_string.split(/(\d+)/)
// console.log(split_string[1])
// //______________________________________
//WAP TO SAPRATE NUMBER IN A GIVEN STRING
// "use strict"
// let abc = "ahdf45fdf14frf52";
// let arr = [];
// let reg = (/\d+/g);
// let result = abc.match(reg);
// console.log(result);
// for(var i=0; i<result.length; i++){
//  arr.push(parseInt(result[i]));
// }
// console.log(arr);
// var a = arr.reduce((a,b) => a+b)
// console.log(a);

//_________________________________________
// or
// function sumDigitsFromString(str) {
//     var sum = 0;
//     var numbers = str.match(/\d+/g).map(Number);
//     for (var i = 0; i < numbers.length; i++) {
//     sum += numbers[i]
//     }
//     console.log(sum);
//     }
//     sumDigitsFromString('12gddjhj5hgfhj6hd11')  

// let p = document.createElement('p');
// let body = document.querySelector('body')
// p.innerHTML = 'sum of element' = + result;
// body.appendChild(p);
