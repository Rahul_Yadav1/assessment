const fs = require('fs')
// const book ={
//     title: 'Ego is the enemy',
//     author: 'Ryan holiday'
// }
// const bookJSON = JSON.stringify(book)
// // console.log(bookJSON)

// // const parseData = JSON.parse(bookJSON)
// // console.log(parseData.author)

// fs.writeFileSync('1-json.json',bookJSON)

// const dataBuffer = fs.readFileSync('1-json.json')
// const dataJSON = dataBuffer.toString()
// // console.log(dataBuffer.toString())
// const data = JSON.parse(dataJSON)
// console.log(data.title)

// //TASK
const dataBuffer = fs.readFileSync('1-json.json')
const dataJSON = dataBuffer.toString()
const user = JSON.parse(dataJSON)
user.name = 'Gunther'
user.age = 54
const userJSON = JSON.stringify(user)
fs.writeFileSync('1-json.json',userJSON)