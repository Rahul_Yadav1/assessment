const path = require("path");
const express = require("express");
const hbs = require("hbs");
const geocode = require("./utils1/geocode");
const forecast = require("./utils1/forecast");

console.log(__dirname);
console.log(path.join(__dirname, "../public"));

const app = express();

//Define paths for Express config
const publicDirectory = path.join(__dirname, "../public");
const viewsPath = path.join(__dirname, "../templates/views");
const partialsPath = path.join(__dirname, "../templates/partials");

//setup handlebars engine and views location
app.set("view engine", "hbs");
app.set("views", viewsPath);
hbs.registerPartials(partialsPath);

//setup static directory to serve
app.use(express.static(publicDirectory));

app.get("", (req, res) => {
  res.render("index", {
    title: "Weather App",
    name: "Rahul"
  });
});

app.get("/about", (req, res) => {
  res.render("about", {
    title: "About me",
    name: "Rahul"
  });
});
//55 The Query String
app.get("/products", (req, res) => {
  if (!req.query.search) {
    return res.send({
      error: "you must provide a search tern"
    });
  }

  console.log(req.query.search);
  res.send({
    products: []
  });
});

app.get("/help", (req, res) => {
  res.render("help", {
    helpText: "This is some helpful text!",
    title: "Help",
    name: "rahul"
  });
});

// app.get('', (req,res) => {
//     res.send('<h1>Weather</h1>')
// })

app.get("/weather", (req, res) => {
  if (!req.query.address) {
    return res.send({
      error: "you must provide a search tern"
    });
  }

  geocode(req.query.address, (error, geocode) => {
    if (error) {
      return res.send({ error })
    }

    forecast(geocode.latitude, geocode.longitude, (error, forecastData) => {
      if (error) {
        return res.send({ error });
      }

      res.send({
        forecast: forecastData,
        address: req.query.address
      });
    });
  });

  // res.send([{
  //     forecast: "It is showing",
  //     location: "indore",
  //     address: req.query.address
  // }])
});

app.get("*", (req, res) => {
  res.send("MY 404 page");
});

// app.com
//app.com/help
//app.com/about

//challenges
//setup an about route and rander a page title
//setup the weathre route and render a page title
app.listen(3000, () => {
  console.log("server is up on port 3000.");
});
