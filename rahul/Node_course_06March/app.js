// const fs = require('fs')

// // fs.appendFileSync('notes.txt','==thank you!!')

// ////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// // const validater = require('validator')
// const notes = require("./utils.js")

// // const sum = getNotes(4, 5)

// // console.log(sum)
// // console.log(validater.isEmail('andrew@example.com'))
// // console.log(validater.isURL("https://mead.io"))
// // ///////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// // chalk
// const chalk = require('chalk')
// // const greenMsg = chalk.green("success!")
// // console.log(greenMsg)
// // console.log(chalk.bold.bgRed.inverse.yellow("warning!"))

// // console.log(chalk.red("MOst Danger!"))

// ////////////////////////////////////////////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// // GETTING INPUT USER

// // console.log(process.argv[2]);

// // const command = process.argv[2]
// // console.log(process.argv)
// // if(command == "add"){
// //     console.log('Adding notes!')
// // }else if(command == 'remove'){  
// //     console.log('Remove notesing!')
// // }
// //Argument parsing with yargs part I
// const yargs = require('yargs')
// //Create add command
// // yargs.command({
// //     command: 'add',
// //     describe: 'add a new notes',
// //     handler: function () {
// //         console.log("Adding a new notes!")
// //     }
// // })

// // //create remove command
// // yargs.command({
// //     command: 'remove',
// //     describe: 'Remove a notes',
// //     handler: function () {
// //         console.log("Removing the notes")
// //     }
// // })
// // ////create read command
// // yargs.command({
// //     command: 'read',
// //     describe: 'Read a notes',
// //     handler: function () {
// //         console.log("Read the notes")
// //     }
// // })
// // //create list command
// // yargs.command({
// //     command: 'list',
// //     describe: 'List a notes',
// //     handler: function () {
// //         console.log("List the notes")
// //     }
// // })
// // console.log(process.argv)
// // console.log(yargs.argv)

// //Argument parsing with yargs part II
// //Create add command
// // yargs.command({
// //     command: 'add',
// //     describe: 'add a new notes',
// //     builder:{
// //         title:{
// //             describe:"Note title",
// //             demandOption: true,
// //             type: 'string'
// //         },
// //         body:{
// //             describe:"Note title",
// //             demandOption: true,
// //             type: 'string'
// //         }
// //     },
// //     handler: function (argv) {
// //         // console.log("Adding a new notes!",argv)
// //         console.log("Title:" +argv.title)
// //         console.log('Body:'+ argv.body)
// //     }
// // })
// // console.log(yargs.argv)
///////////////////////////////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//Adding notes

// Create add command
const notes = require("./utils.js");
const yargs = require("yargs");
yargs.command({
command: "add",
describe: "Add a new Note",
builder: {
title: {
describe: "Note Title",
demandOption: true,
type: "string"
},
body: {
desccribe: "Body of the Note",
demandOption: true,
type: "string"
}
},
handler: function(argv) {
notes.addNote(argv.title, argv.body);
}
});


// create remove command
yargs.command({
command: "remove",
describe: "Remove a Note",
builder: {
title: {
describe: "Note Title",
demandOption: true,
type: "string"
}
},
handler: function(argv) {
notes.removeNotes(argv.title);
}
});

//create list command
yargs.command({
    command: 'list',
    describe: 'List a notes',
    handler() {
       notes.listNotes()
    }
})

yargs.parse();
