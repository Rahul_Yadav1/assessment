const request = require('request')


const geocode = (address,callback)=>{
    const url = "https://api.mapbox.com/geocoding/v5/mapbox.places/' + address + '.json?access_token=pk.eyJ1IjoicmFodWx5YWRhdjEiLCJhIjoiY2s3bXQzdmx4MDBkejNscWExanN1N3J3MCJ9.yW8mFcLu7cjUXujbAj_0IQ"

    request({ url: url, json:true}, (error ,{ body }) =>{
      if(error){
           callback('unable to connect to location srvice!',undefined)
         
      }else if(body.features.length=== 0){
        callback('unable to find location.try another srvice!',undefined)
    }  else{
        callback(undefined,{
             latitude: body.features[0].center[0],
            longitude: body.features[0].center[1],
            location: body.features[0].place_name 
        })
    }
    })
}

module.exports = geocode