//Asynchronous Basic
// console.log('starting')

// setTimeout(() => {
//    console.log("2second timer")
// }, 0)

// console.log('stoping')
// --------------------------------------------------------------------------------------

const request = require('request')

const geocode = require('./utils1/geocode')
const forecast = require('./utils1/forecast')

// const url = 'https://api.darksky.net/forecast/e2ec3d04ab039e6869cf736ce39fdcae/22.7196,75.8577?units=si'

// request({ url: url, json: true}, (error, response) => { 
//     //const data = JSON.parse(response.body) 
//     // console.log(response.body.currently)
//     console.log(response.body.daily.data[0].summary + 'it is currently' + response.body.currently.temperature +'  degress out. there is a' + response.body.currently.precipProbability+'% chance of rain')
// })

//An HTTP Request Challenge
//Geocoding
//address  -> lat/lon ->Weather

// const geocode = "https://api.mapbox.com/geocoding/v5/mapbox.places/Los%20Angeles.json?access_token=pk.eyJ1IjoicmFodWx5YWRhdjEiLCJhIjoiY2s3bXQzdmx4MDBkejNscWExanN1N3J3MCJ9.yW8mFcLu7cjUXujbAj_0IQ"

// request({url: geocode, json: true}, (error,response) =>{
//     const latitude = response.body.features[0].center[0]
//     const longitude = response.body.features[0].center[1]
//     console.log(latitude,longitude);
// })
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Callback Abstraction(is code ko mene geocode ki file me likh diya or waha pr ki exports kar li yaha pr only call kar raha hun)
// const geocode = (address,callback)=>{
//     const url = "https://api.mapbox.com/geocoding/v5/mapbox.places/' + address + '.json?access_token=pk.eyJ1IjoicmFodWx5YWRhdjEiLCJhIjoiY2s3bXQzdmx4MDBkejNscWExanN1N3J3MCJ9.yW8mFcLu7cjUXujbAj_0IQ"

//     request({ url: url, json:true}, (error ,response) =>{
//       if(error){
//            callback('unable to connect to location srvice!',undefined)
         
//       }else if(response.body.features.length=== 0){
//         callback('unable to find location.try another srvice!',undefined)
//     }  else{
//         callback(undefined,{
//              latitude: response.body.features[0].center[0],
//             longitude: response.body.features[0].center[1],
//             location: response.body.features[0].place_name 
//         })
//     }
//     })
// }
// ---------------------------------------------------------------------------------------------------------------------------------------------------------

// geocode('Boston',(error,data)=>{
 
//     console.log('error',error)
//     console.log('Data',data)
// })
// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------

const address = process.argv[2]

if (!address) {
    console.log('Please provide an address')
} else {
    geocode(address, (error, { latitude, longitude, location }) => {
        if (error) {
            return console.log(error)
        }

        forecast(latitude, longitude, (error, forecastData) => {
            if (error) {
                return console.log(error)
            }

            console.log(location)
            console.log(forecastData)
        })
    })
}