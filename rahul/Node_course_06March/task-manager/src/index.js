const express = require('express')
require('./db/mongoose')
const User = require('./models/user')
const Task = require('./models/task')

const app = express()
const port = process.env.PORT || 3000

app.use(express.json())

app.post('/users', (req, res) =>{
    const user = new User(req.body)

   user.save().then(() => {
      res.send(user)
   }).catch((e) =>{
    //    res.send(400)
      res.send(e)
   })
})


app.post('/task', (req, res) =>{
    const user = new Task(req.body)

   user.save().then(() => {
      res.send(user)
   }).catch((e) =>{
    //    res.send(400)
      res.send(e)
   })
})




// app.post('/task', (req,res) => {
//     const task = new Task(req.body)

//     task.save().then(() =>{
//         res.send(task)

//     }).catch((error) =>{

//        console.log(error)
//     })
// })


app.listen(port,() =>{
    console.log('server is up on port'+ port)
})

