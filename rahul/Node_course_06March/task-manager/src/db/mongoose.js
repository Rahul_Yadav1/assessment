const mongoose = require('mongoose')

mongoose.connect('mongodb://127.0.0.1:27017/task-manager-api',{
 useNewUrlParser: true,
 useCreateIndex: true,
 useUnifiedTopology: true   
})

// //this code for Check the validation true or not
// const me = new User({
//    name: '   Rahul  ',
//    age: 24,
//    password: ' 12345678  ',
//    email: 'RAHUL@GMAIL.COM '
// })
// //applay promises && save the instance of the database
// me.save().then((me)=>{
// console.log(me)
// }).catch((error)=>{
//     console.log('error',error)
// })

//task

// const Task = mongoose.model('Task',{
//     description: {
//         type: String,
//         require: true,
//         trim: false
//     },
//     completed: {
//         type: Boolean,
//         default: false
//     }
// })

// const task = new Task({
//     description: '      Eat lunch',
//     // completed: false
// })

// task.save().then(() => {
//     console.log(task)
// }).catch((error) =>{
//     console.log(error)
// })


