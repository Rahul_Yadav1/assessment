// console.log("Call Back Function");
// //call back function in js
// const students = [
//     {name: "Rahul", subject: "C"},
//     {name: "Aman", subject: "Js"}
//  ]

//  function  enrollstudent(student, callback){
//      setTimeout(function(){
//          students.push(student);
//          console.log("student has been enrolled")
//         //  callback();
//      },1000);
//  }

//  function getStudents(){
//      setTimeout(function(){
//          let str = "";
//          students.forEach(function(student){
//              str += `<li> ${student.name} </li>`

//          });
//          document.getElementById('student').innerHTML =str;
//          console.log("student have been fetched");
//      },3000)
//  }
//  let newStudent = {name: "Naman", subject: "java"};
//  enrollstudent(newStudent, getStudents);
//  getStudents();
// __________________________________________________________________________________________
//async and wait

// async function rahul(){
//     console.log('inside aswait function');
//     const response = await fetch('https://api.github.com/users');
//     console.log('before response');
//     const users = await response.json();
//     console.log('users resolved')
//     return users;
// }

// console.log('before calling aswait');
// let a = rahul();
// console.log(a);
// a.then(data => console.log(data))
// console.log("last line of the js file")
//_________________________________________________________________________________________________

//try and catch
let a ="rahul";
a =undefined;
if(a!=undefined){
    throw new Error('this is not undefine');
}
else{
    console.log("we are inside try block");
}

try {
    console.log("we are inside try block");
    functionabc()
} catch(error){
    console.log(error)
    console.log("are you okey")
    console.log(error.name);
    console.log(error.message);
}finally{
    console.log("finally we will run this")
}