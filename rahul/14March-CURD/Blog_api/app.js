const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const path = require("path");

const app = express();

const db = require("./config/db").database;

//Database Connection

mongoose
  .connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Database Connected Successfully");
  })
  .catch(err => {
    console.log("unable to connect with database", err);
  });

//Defining the Port
const port = process.env.Port || 3000;

//intialize cors Middleware
app.use(cors());

//Initialize body parser middleware
app.use(bodyParser.json());

//Initilaize public Directory
/* 
   app.get('*', (req, res) =>{
       res.sendFile(path.join(__dirname, 'public/index.html')); 
    })
    */

app.get("/", (req, res) => {
  res.send("<h1>Hello World</h1>");
});

const postRoutes = require("./routes/apis/post");
app.use("/api/posts", postRoutes);

app.listen(port, () => {
  console.log("Server Started on port", port);
});
